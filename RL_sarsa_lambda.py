import numpy as np
import pandas as pd


class rlalgorithm:

    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9, e_greedy=0.01, lam=0.9):
        self.actions = actions
        self.lam = lam
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.expectation=pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="Sarsa Lambda"

    '''Choose the next action to take given the observed state using an epsilon greedy policy'''
    def choose_action(self, observation, learn = True):
        self.check_state_exist(observation)
 
        if np.random.uniform() >= self.epsilon or (not learn):
           
            state_action = self.q_table.loc[observation, :]
           
            action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        else:
         
            action = np.random.choice(self.actions)
        return action


    '''Update the Q(S,A) state-action value table using the latest experience
    '''
    def learn(self, s, a, r, s_):
        self.check_state_exist(s_)
        if s_ != 'terminal':
            a_ = self.choose_action(str(s_))
            q_target = r + self.gamma * self.q_table.loc[s_, a_] 
        else:
            q_target = r  # next state is terminal
        
        # update q_table using eligibility trace
        error_value = q_target - self.q_table.loc[s, a]

        self.expectation.loc[s, a] += 1
        # Decay for Q and E

        self.q_table += self.lr * error_value * self.expectation # update state-action value for all states and actions

        # update eligibility traces
        if s_ != 'terminal':
            self.expectation *= self.gamma * self.lam 
        else:
            # clear it once episode is done.
            self.expectation = pd.DataFrame(0, 
                                            columns=self.q_table.columns, 
                                            index=self.q_table.index)
            return s_, None
        return s_, a_
    
    '''States are dynamically added to the Q(S,A) table as they are encountered'''
    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table.columns,
                    name=state,
                )
            )
        if state not in self.expectation.index:
            # append new state to expectation table
            self.expectation = self.expectation.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.expectation.columns,
                    name=state,
                )
            )
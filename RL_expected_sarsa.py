import numpy as np
import pandas as pd


class rlalgorithm:

    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9, e_greedy=0.01):
        self.actions = actions  
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="Expected Sarsa"

    '''Choose the next action to take given the observed state using an epsilon greedy policy'''
    def choose_action(self, observation, learn = True):
        self.check_state_exist(observation)
 
        if np.random.uniform() >= self.epsilon and learn:
           
            state_action = self.q_table.loc[observation, :]
           
            action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        else:
         
            action = np.random.choice(self.actions)
        return action


    '''Update the Q(S,A) state-action value table using the latest experience
    '''
    def learn(self, s, a, r, s_):
        self.check_state_exist(s_)
        if s_ != 'terminal':
            number_of_actions = len(self.actions)

            all_action_values_under_s_ = self.q_table.loc[s_,:]
            max_value = np.max(all_action_values_under_s_)

            number_of_max_value = np.sum(all_action_values_under_s_ == max_value)

            expected_value_for_max =  ((1 - self.epsilon)  / number_of_max_value + self.epsilon / number_of_actions) * number_of_max_value * max_value
            expected_value_for_non_max =  (self.epsilon/(number_of_actions-number_of_max_value)) * np.sum(all_action_values_under_s_[all_action_values_under_s_!=max_value])
            if np.isnan(expected_value_for_max):
                expected_value_for_max = 0
            if np.isnan(expected_value_for_non_max):
                expected_value_for_non_max = 0
            expected_value = expected_value_for_max + expected_value_for_non_max
            q_target = r + self.gamma * expected_value 

        else:
            q_target = r  # next state is terminal

        # If epsilon greedy:
        #   Exp(Q(S', A')) = Probability of Argmax_a Q(S',a) +Probabiliy of other actions
        # Q(S, A) = Q(S, A) + learning rate * [r + gamma * Exp(Q(S', A')) - Q(S ,A)]  
        # q_target = r + gamma * Q(S',A')  
        self.q_table.loc[s, a] +=  self.lr * (q_target - self.q_table.loc[s, a])
        return s_, self.choose_action(str(s_))

    '''States are dynamically added to the Q(S,A) table as they are encountered'''
    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table.columns,
                    name=state,
                )
            )

import numpy as np
import pandas as pd


class rlalgorithm:

    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9, e_greedy=0.01):
        self.actions = actions  
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q1_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.q2_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="Double Q learning"

    '''Choose the next action to take given the observed state using an epsilon greedy policy'''
    # estimate == 0: derive from 2 estimate
    #          == 1: use estimate 1
    #          == 2: use estimate 2
    def choose_action(self, observation, learn = True, estimate = 0):
        self.check_state_exist(observation)
 
        if np.random.uniform() >= self.epsilon and learn:
           
            state_action = self.q1_table.loc[observation, :] + self.q2_table.loc[observation, :]
            if estimate == 1:
                state_action = self.q1_table.loc[observation, :]
            elif estimate == 2:
                state_action = self.q2_table.loc[observation, :]
            action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        else:
         
            action = np.random.choice(self.actions)
        return action


    '''Update the Q(S,A) state-action value table using the latest experience
    '''
    def learn(self, s, a, r, s_):
        self.check_state_exist(s_)
        if np.random.uniform() < 0.5:
            if s_ != 'terminal':
                a_ = self.choose_action(str(s_))
                a1_ = self.choose_action(str(s_), estimate=1)
                q_target = r + self.gamma * self.q2_table.loc[s_,a1_]
            else:
                q_target = r  # next state is terminal

            # Q(S, A) = Q(S, A) + learning rate * [r + gamma * max_a Q(S', A') - Q(S ,A)]  
            # q1_target = r + gamma * Q(S',A')  
            self.q1_table.loc[s, a] +=  self.lr * (q_target - self.q1_table.loc[s, a])
        else:
            if s_ != 'terminal':
                a_ = self.choose_action(str(s_))
                a2_ = self.choose_action(str(s_), estimate=2)
                q_target = r + self.gamma * self.q1_table.loc[s_,a2_]
            else:
                q_target = r  # next state is terminal

            # Q_2(S, A) = Q_2(S, A) + learning rate * [r + gamma * Q_1(S',argmax_a Q_2(S', a)) - Q_2(S ,A)]  
            self.q2_table.loc[s, a] +=  self.lr * (q_target - self.q2_table.loc[s, a])
        if s_ == 'terminal':
            return s_, None
        return s_, a_

    '''States are dynamically added to the Q(S,A) table as they are encountered'''
    def check_state_exist(self, state):
        if state not in self.q1_table.index:
            # append new state to q table
            self.q1_table = self.q1_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q1_table.columns,
                    name=state,
                )
            )

        if state not in self.q2_table.index:
            # append new state to q table
            self.q2_table = self.q2_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q2_table.columns,
                    name=state,
                )
            )

import numpy as np
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.autograd import Variable
from torch.distributions import Categorical

class rl_policy(nn.Module):
    def __init__(self, actions):
        super(rl_policy, self).__init__()
        self.state_space = 4 # 4 coordinates
        self.action_space = len(actions) # 4 actions
        
        self.l1 = nn.Linear(self.state_space, 128, bias=False)
        self.l2 = nn.Linear(128, self.action_space, bias=False)
        self.policy_history = Variable(torch.Tensor()) 

    def forward(self, x):    
        model = torch.nn.Sequential(
            self.l1,
            nn.Dropout(p=0.6),
            nn.ReLU(),
            self.l2,
            nn.Softmax(dim=-1)
        )
        return model(x)

    def select_action(self, in_state):
        #Select an action (0 or 1) by running policy model and choosing based on the probabilities in state
        state = torch.from_numpy(np.array(in_state)).type(torch.FloatTensor)
        state = self.forward(Variable(state))
        c = Categorical(state)
        action = c.sample()
        
        return action, c.log_prob(action).reshape(1)

    
class rlalgorithm:

    def __init__(self, actions, learning_rate=0.01, reward_decay=0.99, e_greedy=0.01):
        self.actions = actions  
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="Reinforce"

        self.policy = rl_policy(self.actions)
        self.optimizer = optim.Adam(self.policy.parameters(), lr=self.lr)

        self.reward_history = []
        #self.action_history = []
        #self.state_history = []


    '''Choose the next action to take given the observed state using an epsilon greedy policy'''
    def choose_action(self,observation):
        action, prob = self.choose_action_and_porb(observation)
        return action

    def choose_action_and_porb(self, observation):
        in_state = list(map(float, observation.strip('][').split(',')))
        action, prob = self.policy.select_action(in_state)
        return action.data, prob


    '''Update the Q(S,A) state-action value table using the latest experience
    '''
    def learn(self, s, a, r, s_):
        #self.state_history.append(s)
        #self.action_history.append(a)
        self.reward_history.append(r)
        if s_ != 'terminal':
            a_,prob = self.choose_action_and_porb(str(s_))
            # Record policy histroy
            if len(self.policy.policy_history) > 0:
                self.policy.policy_history = torch.cat([self.policy.policy_history, prob])
            else:
                self.policy.policy_history = prob
        else:
            if len(self.policy.policy_history) != len(self.reward_history):
                self.reward_history.pop()
            # Calculate Return from reward in each episode
            total_reward = []
            for i, item in enumerate(reversed(self.reward_history)):
                if i != 0:
                    total_reward.append(item + self.gamma *total_reward[i-1])
                else:
                    total_reward.append(item)
            total_reward = total_reward[::-1] # reverse total reward

            total_reward = torch.FloatTensor(total_reward)
            total_reward = (total_reward - total_reward.mean()) / (total_reward.std() + float(np.finfo(np.float32).eps))

            # Calculate loss
            loss = (torch.sum(torch.mul(self.policy.policy_history, Variable(total_reward)).mul(-1), -1))
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()
            
            self.policy.policy_history = Variable(torch.Tensor())
            self.reward_history= []
            return s_, None
        # Q(S, A) = Q(S, A) + learning rate * [r + gamma * max_a Q(S', A') - Q(S ,A)]  
        # q_target = r + gamma * Q(S',A')  
        #self.q_table.loc[s, a] +=  + self.lr * (q_target - self.q_table.loc[s, a])
        return s_, a_

